# Build, Run, and Test
```
docker build -t boppy/monit858:1.0 .

docker run -d --name monit858-test boppy/monit858:1.0
sleep 5
docker exec monit858-test monit -V
docker exec monit858-test monit status
```

# Run Bash to Inspect
```
docker exec -ti monit858-test bash
```
