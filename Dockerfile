FROM alpine:3.10
MAINTAINER Henning Bopp <henning.bopp@gmail.com>

RUN apk -q add monit tini bash \
    htop nano

COPY assets /etc/monitrc

COPY assets  /usr/local/sbin/servicectl
COPY assets /usr/local/sbin/fake_daemon

RUN chmod 0600 /etc/monitrc \
    && chmod +x /usr/local/sbin/servicectl \
    && chmod +x /usr/local/sbin/fake_daemon

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/bin/monit", "-I"]
