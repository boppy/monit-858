#!/usr/bin/env bash
if (( ${EUID:-0} || $(id -u) )); then
    sudo="sudo "
fi

case $1 in
    daemon)
        process_name="tail"
        process_binary="/bin/busybox" # tail is emulated by busybox on alpine...

        pid_file="/run/daemon.pid"

        start_command="/usr/local/sbin/fake_daemon"
        reload_command=0
        reload_sig="HUP"
        restart_command=0
        restart_sig=0
        stop_command=0
        stop_sig="KILL"
        ;;

    *)
        echo "Program $1 not configured." >> /dev/stderr
        exit 1
        ;;
esac

function replaceCmd {
    local ret="$1"
    ret="${ret/_PID_FILE_/${pid_file}}"

    local curpid=$(cat "${pid_file}" 2>/dev/null)
    [[ $curpid -ne 0 ]] && ret="${ret/_PID_/${curpid}}"

    echo "$ret"
}
function verifyProcess () {
    local silence="false"
    if [[ "$1" = "-s" ]]; then
        silence="true"
    fi
    local pid=$(cat ${pid_file})
    local is_binary=$(${sudo}readlink /proc/${pid}/exe)

    if [[ "${is_binary}" = "${process_binary}" ]]; then
        $silence || echo "OK: ${process_name} is running as \"${is_binary}\" with PID ${pid}" >> /dev/stderr
        exit 0
    fi

    $silence || echo "FAIL: PIDFILE doesn't match with process name for ${process_name}" >> /dev/stderr
    $silence || echo "  is    : ${is_binary}" >> /dev/stderr
    $silence || echo "  should: ${process_binary}" >> /dev/stderr
    exit 1
}

case $2 in
    status)
        verifyProcess
        ;;

    start)
        if (verifyProcess -s); then
            echo "Process already running" >> /dev/stderr
            exit 1
        fi
        eval "${start_command}"
        ;;

    stop)
        if ! (verifyProcess -s); then
            echo "Process not running" >> /dev/stderr
            exit 1
        fi

        if [[ ${stop_command} -ne 0 ]]; then
            command=$(replaceCmd "${stop_command}")
            eval "${command}"
            status=$?
            [[ ${status} -eq 0 && -f ${pid_file} ]] && rm -f ${pid_file} 2>/dev/null
            exit ${status}
        fi

        pid=$(cat ${pid_file})
        kill -${stop_sig:-KILL} ${pid}
        ;;

    reload)
        if ! (verifyProcess -s); then
            echo "Process not running" >> /dev/stderr
            exit 1
        fi

        if [[ ${reload_command} -ne 0 ]]; then
            command=$(replaceCmd "${reload_command}")
            eval "${command}"
        elif [[ ${reload_sig} -ne 0 ]]; then
            pid=$(cat ${pid_file})
            kill -${reload_sig} ${pid}
        else
            $0 $1 restart
        fi
        ;;

    restart)
        if ! (verifyProcess -s); then
            "$0" "$1" start
            exit $?
        fi

        if [[ ${restart_command} -ne 0 ]]; then
            command=$(replaceCmd "${restart_command}")
            eval "${command}"
        elif [[ ${restart_sig} -ne 0 ]]; then
            pid=$(cat ${pid_file})
            kill -${reload_sig} ${pid}
        else
            if "$0" "$1" stop; then
                "$0" "$1" start
            else
                echo "Cannot stop. Aborted restart" >> /dev/stderr
                exit 1
            fi
        fi
        ;;

    *)
        echo "Command $2 for Program $1 not configured." >> /dev/stderr
        exit 1
        ;;
esac
